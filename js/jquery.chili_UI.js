;(function ($) {
    /**
     * @summary 获取 jQuery 对象中 data 存储数据的某个数据
     *
     * @param  {string} property 数据的 key
     * @return {Object}          数据的 value
     */
    var getProperty = function (property, ele) {
		for(var key in ele.data('property')) {
			if (key == property) {
				return ele.data('property')[key];
			}
		}
		return null;
	};

    var util = function () {
        this.eid = 0;
    };
    
    
    util.prototype.getId = function (prefix) {
        var pre = prefix || 'id';
        var str = pre + this.eid;
        this.eid++;
        return str;
    };

    /**
     * @namespace chili
     */
    chili       =   {};

    /**
     * @namespace chili.UI
     */
    chili.UI    =   {};

    /**
     * 插件基本信息
     *
     * @type {Object}
     */
    chili.UI.info = {
        version :   'v2.0',
        author  :   'ludong',
        date    :   '2016-10-24 10:43:04',
        email   :   '9821301ld@163.com',
        QQ      :   '458989005'
    };

    /**
     * @class
     * @classdesc button 对象
     * @param  {[jQuery]} ele [jQuery 对象]
     *
     * @example
     * var button1 = new chili.UI.button($('#div1'));
     */
    chili.UI.button = function (ele) {
        this.element = ele;
    };

    /**
     * @summary button 初始化
     * @param  {[Object]} [opt] [button 的参数集]
     * width       :   100,            // 按钮的宽度
     * height      :   40,             // 按钮的高度
     * fontSize    :   14,             // 字体大小
     * fontColor   :   '#FFF',         // 默认字体颜色
     * fontColor1  :   '#FFF',         // 鼠标移上去字体的颜色
     * fontFamily  :   'arial',        // 字体
     * bgColor     :   'royalblue',    // 背景颜色
     * fgColor     :   'dodgerblue',   // 前景颜色
     * caption     :   'button',       // 按钮标签
     * display     :   'inline-block', // 显示方式
     * tran_speed  :   '0.1s',         // 颜色过渡时间
     * shadow      :   '10px',         // 阴影大小
     * radius      :   4               // 按钮圆角半径
     *
     * @example
     * var button1 = new chili.UI.button($('#div1'));
     * var button2 = new chili.UI.button($('#div2'));
     * button1.init();
     * button2.init({width : 40, radius : 20, caption : 'Yes'});
     */
    chili.UI.button.prototype.init = function (opt) {
        var defaults = {
            width       :   100,            // 按钮的宽度
            height      :   40,             // 按钮的高度
            fontSize    :   14,             // 字体大小
            fontColor   :   '#FFF',         // 默认字体颜色
            fontColor1  :   '#FFF',         // 鼠标移上去字体的颜色
            fontFamily  :   'arial',        // 字体
            bgColor     :   'royalblue',    // 背景颜色
            fgColor     :   'dodgerblue',   // 前景颜色
            caption     :   'button',       // 按钮标签
            display     :   'inline-block', // 显示方式
            tran_speed  :   '0.1s',         // 颜色过渡时间
            shadow      :   '10px',         // 阴影大小
            radius      :   4               // 按钮圆角半径
        };
        var ele = this.element;
        var options = $.extend({}, defaults, opt);
        this.element.data('property', options);
        ele.each(function(){
            var _this = $(ele);
            var str = 'all ' + options.tran_speed + ' ease-in-out';
            _this.html('<span>' + options.caption + '</span>');
            _this.css({
                'width'                 :   options.width + 'px',
                'height'                :   options.height + 'px',
                'font-family'           :   options.fontFamily,
                'color'                 :   options.fontColor,
                'font-size'             :   options.fontSize + 'px',
                'background-color'      :   options.bgColor,
                'text-align'            :   'center',
                'text-overflow'         :   'clip',
                'cursor'                :   'pointer',
                'border-radius'         :   options.radius + 'px',
                'transition'            :   str,
                '-webkit-transition'    :   str,
                '-moz-transition'       :   str,
                'display'               :   options.display
            });
            _this.find('span').css('line-height', options.height + 'px');
            _this.mouseover(function(){
                str = '0 0 ' + options.shadow + ' ' + options.fgColor;
                _this.css({
                    'box-shadow'            :   str,
                    '-webkit-box-shadow'    :   str,
                    '-moz-box-shadow'       :   str,
                    '-o-box-shadow'         :   str,
                    '-ms-box-shadow'        :   str,
                    'background-color'      :   options.fgColor,
                    'color'                 :   options.fontColor1
                });
                _this.css('box-shadow',str);
            }).mouseout(function(){
                str = 'none';
                _this.css({
                    'background-color'      :   options.bgColor,
                    'box-shadow'            :   str,
                    '-webkit-box-shadow'    :   str,
                    '-moz-box-shadow'       :   str,
                    '-o-box-shadow'         :   str,
                    '-ms-box-shadow'        :   str,
                    'color'                 :   options.fontColor
                });
            });
        });
    };

    /**
     * @summary 获取 button 对象的属性
     * @param  {string} property [要获取属性的 key]
     * @return {any}          [要获取属性的 value]
     */
    chili.UI.button.prototype.getProperty = function (property) {
        return getProperty(property, this.element);
    };

    /**
     * @summary 窗口框架
     * @param  {jQuery} ele [jQuery 对象]
     */
    chili.UI.form = function (ele) {
    	this.element = ele;
    	this.methods = {};
    }

    /**
     * @summary [初始化]
     * @param  {[Object]} opt [description]
     * 
     */
    chili.UI.form.prototype.init = function (opt) {
    	var defaults = {
    		width : 400,
    		height : 300,
    		location : {x:0, y:0},
    		center : true,
    		z_index : 1000,
    		title : "窗口1",
    		speed : 300
    	};
    	options = $.extend({}, defaults, opt);
    	if (options.center) {
    		options.location.x = ($(window).width() - options.width) / 2;
    		options.location.y = ($(window).height() - options.height) / 2;
    	}
    	this.element.data('property', options);
    	var ele = this.element;
    	var id = ele.attr("id");
    	var templateStr = `
    			<div id="${id}_mask"></div>
				<div id="${id}_main">
					<div id="${id}_titDiv">
						<span id="${id}_tit"></span>
						<div id="${id}_butDiv">
							<div id="${id}_butt1"></div>
						</div>
					</div>
					<div id="${id}_view">
					</div>
				</div>
    		`;
    	ele.empty().html(templateStr);

		var mask = $(`#${id}_mask`);
		var main = $(`#${id}_main`);
		var titDiv = $(`#${id}_titDiv`);
		var tit = $(`#${id}_tit`);
		var butDiv = $(`#${id}_butDiv`);
		var view = $(`${id}_view`);
		var closeButton = new chili.UI.button($(`#${id}_butt1`));

		ele.css({
			'display' : 'none'
		});
		mask.css({
			'position'			: 'fixed',
	        'top'				: '0',
	        'left'				: '0',
	        'width'				: $(window).width() + 'px',
	        'height'			: $(window).height() + 'px',
	        'background-color'	: 'rgba(0,0,0,0.5)',
	        'z-index'			: options.z_index
		});

		main.css({
			'position'			: 'fixed',
	        'top'				: options.location.y + "px",
	        'left'				: options.location.x + "px",
	        'width'				: options.width + 'px',
	        'height'			: options.height + 'px',
	        'background-color'	: '#FFF',
	        'z-index'			: options.z_index + 1,
	        'border'        	: 'solid 1px #909090',
	        'border-radius' 	: '5px',
	        'box-shadow'    	: '5px 5px 10px 5px rgba(0,0,0,0.3)',
	        'overflow'      	: 'hidden'
		});

		titDiv.css({
			'width' : (options.width - 20) + 'px',
			'height' : '30px',
			'background-color' : '#CCC',
			'color' : '#000',
			'font-size' : '14px',
			'font-weight' : 'bold',
			'line-height' : '30px',
			'padding-left' : '20px',
			'cursor'	: 'default',
			'user-select' : 'none'
		});

		tit.text(options.title);

		butDiv.css({
			'float' : 'right',
			'margin' : '2px 4px'
		})

		closeButton.init ({
            width		: 26,
            height		: 26,
            fontColor	: '#000',
            bgColor		: '#CCC',
            fgColor		: '#909090',
            caption 	: '×',
            radius		: 20
        });

        closeButton.element.click(function(){
        	ele.toggle(options.speed);
        });
    };

    chili.UI.form.prototype.show = function () {
    	this.init(this.element.data('property'));
    	this.element.toggle(getProperty('speed', this.element));
    }

    chili.UI.form.prototype.getProperty = function (property) {
        return getProperty(property, this.element);
    };

    /**
     * @class
     * @classdesc msgBox 对象
     * @param  {jQuery} ele [jQuery 对象]
     * @example
     * var msgBox = chili.UI.msgBox($('#div1'));
     */
    chili.UI.msgBox = function (ele) {
        this.element = ele;
    };

    /**
     * @summary 弹出消息窗口
     * @param  {[string]}   message [消息内容]
     * @param  {[string]}   title   [消息窗的标题]
     * @param  {Function} fn      [点击确认后要执行的方法]
     * @example
     * var msbBox = chili.UI.msgBox($('#div1'));
     * msBox.show('Hello JavaScript!', 'msg', function () {alert('关闭了消息窗');});
     */
    chili.UI.msgBox.prototype.show = function (message, title, fn) {
        var defaults = {
            msg		:     '',
            tit		:     'chili',
            speed	:     200,
            width	:     $(window).width() * 0.3
        };

        var msg, tit, fun;
        if (message === undefined) {
            msg = '';
        } else {
            msg = message.toString();
        }

        if (fn === undefined) {
            fun = function () {};
        } else {
            fun = fn;
        }

        if (title === undefined) {
            tit = 'chili';
        } else if (typeof title == 'function') {
            tit =  'chili';
            fun = title;
        } else {
            tit = title.toString();
        }

        var opt = {
            msg :   msg,
            tit :   tit,
            fn  :   fun
        };

        var ele = this.element;
        ele.empty();
        var options = $.extend({}, defaults, opt);
        var htmlStr = '<div><div></div><div></div><div><div></div></div><div></div></div><div></div>';
        ele.html(htmlStr);
        var popDiv =  ele.children().first();
        var maskDiv = ele.children().next();
        var titDiv = popDiv.children().first();
        var msgDiv = popDiv.children().first().next();
        var bottomDiv = msgDiv.next();
        var buttonDiv = bottomDiv.children().first();
        var button1Div = popDiv.children().last();
        var bottomDiv_height = 55;
        var zIndex = 10000;
        ele.css({
            'display'	: 'none'
        });
        maskDiv.css({
            'position'			: 'fixed',
            'top'				: '0',
            'left'				: '0',
            'width'				: $(window).width() + 'px',
            'height'			: $(window).height() + 'px',
            'background-color'	: 'rgba(0,0,0,0.5)',
            'z-index'			: zIndex
        });
        popDiv.css({
            'z-index'			: zIndex + 1,
            'width'         	: options.width + 'px',
            'border'        	: 'solid 1px #909090',
            'border-radius' 	: '5px',
            'overflow'      	: 'hidden',
            'position'      	: 'fixed',
            'top'           	: ((($(window).height() - ele.height()) / 2) * 0.6) + 'px',
            'left'          	: (($(window).width() - options.width) / 2) + 'px',
            'box-shadow'    	: '5px 5px 10px 5px rgba(0,0,0,0.3)',
            'background-color'	: '#FFF'
        });
        titDiv.css({
            'color'         : '#000',
            'width'         : (options.width - 40) + 'px',
            'font-size'     : '20px',
            'margin'        : '20px auto 0',
            'border-bottom'	: 'solid 1px #CCC'
        }).html(options.tit);
        msgDiv.css({
            'color'             : '#000',
            'font-size'         : '16px',
            'width'             : (options.width - 40) + 'px',
            'margin'            : '20px auto 0',
            'word-wrap'         : 'break-word',
            'background-color'	: '#FFF'
        }).html(options.msg);
        bottomDiv.css({
            'margin-top'		: '20px',
            'width'             : options.width + 'px',
            'height'            : bottomDiv_height + 'px',
            'background-color'  : '#EEE'
        });
        var button1 = new chili.UI.button(button1Div);
        button1.init ({
            width		: 30,
            height		: 30,
            fontSize	: 24,
            fontColor	: '#000',
            bgColor		: '#FFF',
            fgColor		: '#909090',
            caption 	: '×',
            radius		: 15
        });
        button1.element.css({
            'position'			: 'absolute',
            'top'				: '10px',
            'left'				: (options.width - 40) + 'px',
            'font-weight'		: 'bold'
        });
        button1.element.click(function(){
            ele.fadeOut(options.speed, function(){
                options.fn();
            });
        });
        var button2 = new chili.UI.button(buttonDiv);
        button2.init ({
            width   	: 80,
            height  	: 35,
            bgColor		: 'gray',
            fgColor		: '#909090',
            caption 	: '确定'
        });
        button2.element.css({
            'float'     : 'right',
            'margin'    : ((bottomDiv_height - button2.getProperty('height')) / 2) + 'px 20px 0 0'
        });
        button2.element.click(function(){
            ele.fadeOut(options.speed, function(){
                options.fn();
            });
        });
        ele.data('property', options);
        ele.fadeIn(options.speed);
        document.documentElement.scrollTop = 0;
    };

    /**
     * @summary 获取 msgBox 对象的属性
     * @param  {string} property [要获取属性的 key]
     * @return {any}          [要获取属性的 value]
     */
    chili.UI.msgBox.prototype.getProperty = function (property) {
        return getProperty(property, this.element);
    };

    chili.UI.tip = function (ele) {
        this.element = ele;
    }

    chili.UI.tip.prototype.show = function (opt) {
        var ele = this.element;
        var defaults = {
            message : "message...",
            zIndex : 1000
        };
        var options = $.extend({}, defaults, opt);
        var htmlStr = "<div><span></span></div>";
        ele.html(htmlStr);

        var msgDiv = ele.children().first();
        var msgSpan = msgDiv.children().first();

        ele.css({
            'position'          : 'fixed',
            'top'               : '0',
            'left'              : '0',
            'width'             : '100%',
            'height'            : '100%',
            'background-color'  : 'rgba(0,0,0,0.5)',
            'z-index'           : options.zIndex
        });
        msgDiv.css({
            "position" : "absolute",
            "text-align" : "center",
            "height" : "50px",
            "padding" : "0 20px",
            "background-color" : "#EFEFEF",
            'top'    : '200px',
            'left'   : '300px',
        });
        msgSpan.css({
            "line-height" : "50px"
        }).html(options.message);
    }

    /**
     * @class
     * @classdesc tree 对象
     * @param  {jQuery} ele [jQuery 对象]
     * @example
     * var tree = chili.UI.tree($('#div1'));
     */
    chili.UI.tree = function (ele) {
        this.element = ele;
    };

    /**
     * @summary 初始化 tree 对象
     * @param  {[Object]} opt [tree 的参数]
     * {
     * global      : {                         // 全局参数设置
     *     width				:  300,        // 树型菜单宽度
     *     unitHeight			:  50,         // 每个单元格的高度
     *     fontFamily			:  'arial',    // 字体
     *     speed				:  200,        // 动画时间（毫秒）
     *     unitBorderWidth	    :  1,          // 边框宽度
     *     unitBorderColor 	    :  '#999',     // 边框颜色
     *     cursor				:  'pointer',  // 鼠标样式
     *     transitionDuration	:  0.2         // 动画速度
     *     },
     * superior    : {                     // 上级菜单参数设置
     *         fontSize		:  16,         // 字体大小
     *         font_bgColor	:  '#CCC',     // 字体背景颜色
     *         font_fgColor	:  '#FFF',     // 字体前景颜色
     *         bgColor		:  '#36393D',  // 单元格背景颜色
     *         fgColor		:  '#222',     // 单元格前景颜色
     *         lightColor	:  'red'       // 标记灯颜色
     *     },
     * children    : {                     // 下级菜单参数设置
     *         fontSize		:  16,         // 字体大小
     *         font_bgColor	:  '#CCC',     // 字体背景颜色
     *         font_fgColor	:  '#FFF',     // 字体前景颜色
     *         bgColor		:  '#543356',  // 单元格背景颜色
     *         fgColor		:  '#543694'   // 单元格前景颜色
     *     }
     * }
     * @example
     * var treeMenu = new chili.UI.tree($('#dvi1'));
     */
    chili.UI.tree.prototype.init = function (opt) {
        var defaults = {
            global      : {                        // 全局参数设置
                width				:  300,        // 树型菜单宽度
				unitHeight			:  50,         // 每个单元格的高度
				fontFamily			:  'arial',    // 字体
				speed				:  200,        // 动画时间（毫秒）
				unitBorderWidth		:  1,          // 边框宽度
				unitBorderColor 	:  '#999',     // 边框颜色
				cursor				:  'pointer',  // 鼠标样式
				transitionDuration	:  0.2         // 动画速度
            },
            superior    : {                        // 上级菜单参数设置
                fontSize		:  16,             // 字体大小
				font_bgColor	:  '#CCC',         // 字体背景颜色
				font_fgColor	:  '#FFF',         // 字体前景颜色
				bgColor			:  '#36393D',      // 单元格背景颜色
				fgColor			:  '#222',         // 单元格前景颜色
				lightColor		:  'red'           // 标记灯颜色
            },
            children    : {                        // 下级菜单参数设置
                fontSize		:  16,             // 字体大小
				font_bgColor	:  '#CCC',         // 字体背景颜色
				font_fgColor	:  '#FFF',         // 字体前景颜色
				bgColor			:  '#543356',      // 单元格背景颜色
				fgColor			:  '#543694'       // 单元格前景颜色
            }
        };

        var opt1 = opt || {};
        var options = {
            global      :  {},
            superior    :  {},
            children    :  {}
        };

        var ele = this.element;
        options.global = $.extend({}, defaults.global, opt1.global);
		options.superior = $.extend({}, defaults.superior,opt1.superior);
		options.children = $.extend({}, defaults.children,opt1.children);
		var superList = ele.children('ul').children('li');
		var childrenList = ele.children('ul').children('ul').find('li');
		if (typeof options.global.width === 'string'){
			ele.css('width', options.global.width);
			options.global.width = ele.width();
		} else {
			ele.css('width', options.global.width + 'px');
		}

		ele.find('ul').css({
			'margin'	: 0,
			'padding'	: 0
		});

		ele.find('li').css({
			'list-style-type'	: 'none',
			'height'			: options.global.unitHeight + 'px',
			'border'			: 'solid ' + options.global.unitBorderWidth + 'px ' + options.global.unitBorderColor,
			'border-top-width'	: 0,
			'cursor'			: options.global.cursor
		}).attr('class', 'transition');

		ele.children('ul').children('li:first').css('border','solid ' + options.global.unitBorderWidth + 'px ' + options.global.unitBorderColor);
		ele.find('span').css({
			'font-family'	: options.global.fontFamily,
			'line-height'	: options.global.unitHeight + 'px'
		}).attr('class', 'transition');
		superList.each(function () {
			var _this = $(this);
			_this.data('Lock',false);
			_this.prepend('<div></div>');
			var lightDiv = _this.children('div:first');
			var textDiv = _this.find('span');
			var childrenUl = _this.next('ul');
			lightDiv.css({
				'height'			: options.global.unitHeight + 'px',
				'width'				: '5px',
				'background-color'	: options.superior.bgColor,
				'float'				: 'left'
			}).attr('class', 'transition');
			_this.css({
				'background-color': options.superior.bgColor
			});
			textDiv.css({
				'margin-left'	: (options.global.width * 0.05) + 'px',
				'font-size'		: options.superior.fontSize + 'px',
				'color'			: options.superior.font_bgColor
			});
			childrenUl.css('display','none');
			_this.bind('mouseover',function(){
				if (!_this.data('Lock')){
					_this.css('background-color',options.superior.fgColor);
					textDiv.css('color',options.superior.font_fgColor);
					lightDiv.css('background-color',options.superior.lightColor);
				}
			}).bind('mouseout', function () {
				if (!_this.data('Lock')) {
					_this.css('background-color', options.superior.bgColor);
					textDiv.css('color', options.superior.font_bgColor);
					lightDiv.css('background-color', options.superior.bgColor);
				}
			}).bind('click',function(){
				if (_this.next().children().is('li')){
					_this.data('Lock',!_this.data('Lock'));
					_this.next('ul').slideToggle(options.global.speed);
					_this.siblings('li').next('ul').slideUp(options.global.speed,function(){
						var li = $(this);
						li.prev().css({
							'background-color'	: options.superior.bgColor
						});
						li.prev().children('span').css({
							'color'				: options.superior.font_bgColor
						});
						li.prev().children('div:first').css({
							'background-color': options.superior.bgColor
						});
						li.prev().data('Lock', false);
					});
				}
			});
		});
		childrenList.each(function () {
			var _this = $(this);
			var textDiv = _this.find('span');
			_this.css({
				'background-color'		: options.children.bgColor
			});
			textDiv.css({
				'margin-left'	: (options.global.width * 0.15) +'px',
				'font-size'		: options.children.fontSize + 'px',
				'color'			: options.children.font_bgColor
			}).attr('class', 'transition');
			_this.bind('mouseover', function () {
				_this.css('background-color',options.children.fgColor);
				textDiv.css('color',options.children.font_fgColor);
			}).bind('mouseout',function(){
				_this.css('background-color',options.children.bgColor);
				textDiv.css('color',options.children.font_bgColor);
			});
		});

		ele.find('.transition').css({
			'transition-property': 'background-color, color',
			'transition-timing-function': 'ease-in-out',
			'transition-duration': options.global.transitionDuration + 's'
		});
    };

    /**
     * 将 Json 格式的数据转换为 html 列表
     * @param  {Json} json [Json 格式的数据]
     * @param  {Object} opt  [参数集]
     * 参数样式：{str:'', id: 0, live: 0}
     * @return {Object}      [{str:string, id: int, live: int}]
     */
    var tree_parsedJson = function (json, opt) {
        var s = opt.str;
        var _level = opt.level;
        var _id = opt.id;
        var options;

        for (var key in json) {
            switch (key) {
                case 'name' :
                    s += '<li';
                    if((json.click !== undefined) && (json.url === undefined)) {
                        s += ' onclick=\"' + json.click + '\"';
                    }
                    if ((json.url !== undefined) && (json.click === undefined)) {
                        if ((json.newWin === undefined) || (json.newWin === false)){
                            s += ' onclick=\"javascript:window.location.href=\'' + json.url + '\'\"';
                        }
                        if(json.newWin === true) {
                            s += ' onclick=\"javascript:window.open(\'' + json.url + '\')\"';
                        }
                    }
                    s += '>';
                    s += '<span>' + json[key];
                    s += '</span></li>';
                    _id++;
                    _level++;
                    break;
                case 'children' :
                    s += '<ul>';
                    options = tree_parsedJson(json[key], {str:s,id:_id,level:_level});
                    s = options.str;
                    _id = options.id;
                    s += '</ul>';
                    break;
                default :
                    if (typeof json[key] == 'object') {
                        options = tree_parsedJson(json[key], {str:s,id:_id,level:_level});
                        s = options.str;
                        _id = options.id;
                    }
            }
        }
        return {str:s, id:_id, level:_level};
    };

    /**
     * @summary 将 Json 转换为 html 列表字符串
     * @param  {Json} json [Json 数据]
     * @return {string}      [html 列表字符串]
     * @example
     * var json =[
     *         {
     *             name: "父节点11 - 折叠",
     *             children: [
     *                 {name: "弹出对话框", click: "alert('ok');"},
     *                 {name: "打开链接", url:"http://www.baidu.com", newWin:false},
     *                 {name: "在新窗口打开链接", url:"http://www.baidu.com", newWin:true},
     *                 {name: "叶子节点114"}
     *             ]
     *         },
     *         {
     *             name: "父节点11 - 折叠",
     *             children: [
     *                 {name: "叶子节点111"},
     *                 {name: "叶子节点112"},
     *                 {name: "叶子节点113"},
     *                 {name: "叶子节点114"}
     *             ]
     *         },
     *         {
     *             name: "父节点11 - 折叠"
     *         },
     *         {
     *             name: "父节点11 - 折叠",
     *             children: [
     *                 {name: "叶子节点111"},
     *                 {name: "叶子节点112"},
     *                 {name: "叶子节点113"},
     *                 {name: "叶子节点114"}
     *             ]
     *         }
     *     ];
     * var treeMenu = new chili.UI.tree($('#div1'));
     * treeMenu.parsedJson(json);
     * treeMenu.init();
     */
    chili.UI.tree.prototype.parsedJson = function (json) {
        var opt = {str:'', id:0, level:0};
        var str = '<ul>' + tree_parsedJson(json, opt).str + '</ul>';
        this.element.html(str);
        return str;
    };

    /**
     * [导航栏按钮]
     * @param {jQuery} ele [jQuery 对象]
     */
    var NavigationBarButton = function (ele) {
        this.element = ele;
    };

    /**
     * [导航栏按钮默认属性]
     * unit_width  :   100,            // 按钮的宽度
     * height      :   40,             // 按钮的高度
     * fontSize    :   14,             // 字体大小
     * fontColor   :   '#FFF',         // 默认字体颜色
     * fontColor1  :   '#FFF',         // 鼠标移上去字体的颜色
     * fontColor2  :   '#FFF',         // 鼠标单击字体的颜色
     * fontFamily  :   'arial',        // 字体
     * bgColor     :   '#65666D',      // 背景颜色
     * fgColor     :   '#55575C',      // 前景颜色
     * fgColor1    :   '#800000',      // 鼠标单击前景色
     * borderColor :   '#4E5155',      // 边框颜色
     * caption     :   'button',       // 按钮标签
     * tran_speed  :   '0.1s',         // 颜色过渡时间
     */
    var navigationBarButtonDefaults = {
        unit_width  :   100,            // 按钮的宽度
        height      :   40,             // 按钮的高度
        fontSize    :   14,             // 字体大小
        fontColor   :   '#FFF',         // 默认字体颜色
        fontColor1  :   '#FFF',         // 鼠标移上去字体的颜色
        fontColor2  :   '#FFF',         // 鼠标单击字体的颜色
        fontFamily  :   'arial',        // 字体
        bgColor     :   '#65666D',      // 背景颜色
        fgColor     :   '#55575C',      // 前景颜色
        fgColor1    :   '#800000',      // 鼠标单击前景色
        borderColor :   '#4E5155',      // 边框颜色
        caption     :   'button',       // 按钮标签
        tran_speed  :   '0.1s',         // 颜色过渡时间
    };

    // 导航栏按钮属性临时变量
    var navigationBarButtonOptions = {};

    /**
     * [导航栏按钮初始化]
     * @param  {[Object]} opt [导航栏按钮属性，详见 navigationBarButtonDefaults 参数]
     */
    NavigationBarButton.prototype.init = function (opt) {
        var options = $.extend({}, navigationBarButtonDefaults, opt, {isClick : false});
        navigationBarButtonOptions = options;
        this.element.data('property', options);
        var ele = this.element;
        ele.each(function() {
            var _this = $(ele);
            var str = 'all ' + options.tran_speed + ' ease-in-out';
            _this.html('<div><span>' + options.caption + '</span></div>');
            _this.css({
                'width'                 :   options.unit_width + 'px',
                'height'                :   options.height + 'px',
                'font-family'           :   options.fontFamily,
                'color'                 :   options.fontColor,
                'font-size'             :   options.fontSize + 'px',
                'background-color'      :   options.bgColor,
                'text-align'            :   'center',
                'text-overflow'         :   'clip',
                'cursor'                :   'pointer',
                'float'                 :   'left',
                'transition'            :   str,
                '-webkit-transition'    :   str,
                '-moz-transition'       :   str,
                'z-index'               :   0
            }).mouseover(function() {
                if (! _this.data('isClick')) {
                    _this.css({
                        'background-color'  :   options.fgColor,
                        'z-index'           :   1,
                        'color'             :   options.fontColor1
                    });
                    _this.find('div').css({
                        'margin-top'    :   '0px',
                        'height'        :   (options.height) + 'px'
                    });
                    _this.find('span').css({
                        'line-height'   :   (options.height) + 'px'
                    });
                }
            }).mouseout(function() {
                if (! _this.data('isClick')) {
                    _this.css({
                        'background-color'  :   options.bgColor,
                        'z-index'           :   0,
                        'color'             :   options.fontColor
                    });
                    _this.find('div').css({
                        'margin-top'    :   ((options.height - (options.height * 0.7)) / 2) + 'px',
                        'height'        :   (options.height *0.7) + 'px'
                    });
                    _this.find('span').css({
                        'line-height'   :   (options.height * 0.7) + 'px'
                    });
                }
            }).click(function() {
                _this.css({
                    'background-color'  :   options.fgColor1,
                    'z-index'           :   1,
                    'color'             :   options.fontColor2
                });
                _this.find('div').css({
                    'margin-top'    :   '0px',
                    'height'        :   (options.height) + 'px'
                });
                _this.find('span').css({
                    'line-height'   :   (options.height) + 'px'
                });

                _this.siblings().css({
                    'background-color'  :   options.bgColor,
                    'z-index'           :   0,
                    'color'             :   options.fontColor
                }).find('div').css({
                    'margin-top'    :   ((options.height - (options.height * 0.7)) / 2) + 'px',
                    'height'        :   (options.height *0.7) + 'px'
                }).find('span').css({
                    'line-height'   :   (options.height * 0.7) + 'px'
                });
                _this.siblings().data('isClick', false);

                _this.data('isClick', true);
            });

            _this.find('div').css({
                'position'      :   'absolute',
                'margin-top'    :   ((options.height - (options.height * 0.7)) / 2) + 'px',
                'width'         :   (options.unit_width - 1) + 'px',
                'height'        :   (options.height * 0.7) + 'px',
                'border-left'   :   'solid 1px ' + options.borderColor,
                'border-right'  :   'solid 1px ' + options.borderColor
            });

            _this.find('span').css({
                'line-height'   :   (options.height * 0.7) + 'px'
            });
        });
    };

    /**
     * [导航栏按钮激活]
     */
    NavigationBarButton.prototype.activate = function () {
        var options = navigationBarButtonOptions || {};
        this.element.css({
            'background-color'  :   options.fgColor1,
            'z-index'           :   1,
            'color'             :   options.fontColor2
        });
        this.element.find('div').css({
            'margin-top'    :   '0px',
            'height'        :   (options.height) + 'px'
        });
        this.element.find('span').css({
            'line-height'   :   (options.height) + 'px'
        });
        this.element.data('isClick', true);
    };

    /**
     * @summary bar 对象
     * @param  {jQuery} ele [jQuery 对象]
     * @example
     * var bar = new chili.UI.bar($('#div1'));
     */
    chili.UI.bar = function (ele) {
        this.element = ele;
    };

    /**
     * @summary bar 对象初始化
     * @param  {json} json [json 数据格式]
     * @param  {[Object]} opt  [参数集]
     * @example
     * var zBar = [
     *      {caption : "主页", click : "", activate : true},
     *      {caption : "微博", click : ""},
     *      {caption : "邮箱", click : ""},
     *      {caption : "联系我们", click : ""},
     *     ];
     * var bar = new chili.UI.bar($('dvi1'));
     * bar.init(zBar, {unit_width : 120, width : "70.55%"}); 
     */
    chili.UI.bar.prototype.init = function (json, opt) {
        var buttons = [];
        var tools = new util();
        var opt1 = opt || {};
        var re = /^[0-9]*\.?[0-9]*%$/;
        if (typeof opt1.width === 'number') {
            opt1.width = opt1.width + 'px';
        } else if ((typeof opt1.width === 'string') && (re.test(opt1.width))) {
            opt1.width = opt1.width;
        }
        else {
            opt1.width = '100%';
        }
        var options = $.extend({}, navigationBarButtonDefaults, opt1);
        var ele = this.element;
        this.element.data('property', options);

        ele.css({
            'width' : options.width,
            'height' : options.height,
            'background-color' : options.bgColor,
            'padding-left' : '15px'
        });

        for (var i = 0; i < json.length; i++) {
            var id = tools.getId('navigationBarButton');
            ele.append('<div id=\"' + id + '\" onclick=\"' + json[i].click + '\"></div>');
            var button = new NavigationBarButton($('#' + id));
            button.element.data('autoId', id);
            buttons.push(button);
            button.init($.extend({}, opt1, {caption : json[i].caption}));
            if (json[i].activate === true) {
                button.activate();
            }
        }
        this.element.data('buttons', buttons);
    };

})(jQuery);
